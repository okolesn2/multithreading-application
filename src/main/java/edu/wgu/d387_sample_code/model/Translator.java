package edu.wgu.d387_sample_code.model;

//The Translator class holds 3 strings and contains setter and getter methods.
public class Translator {
    private String threadName;
    private String idName;
    private String message;
    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }
    public String getThreadName() {
        return threadName;
    }
    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

}
