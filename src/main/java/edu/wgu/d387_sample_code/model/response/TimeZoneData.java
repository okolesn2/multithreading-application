package edu.wgu.d387_sample_code.model.response;

public class TimeZoneData {
    public String timezone;
    public String strTime;

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
    public String getTimezone() {
        return timezone;
    }


    public void setstrTime(String strTime) {
        this.strTime = strTime;
    }
    public String getstrTime() {
        return strTime;
    }

}
