package edu.wgu.d387_sample_code;
import edu.wgu.d387_sample_code.model.response.TimeZoneData;
import java.time.*;

public class Time {



    //the converter method returns the current local time of a given timezone.
    public static TimeZoneData converter(String timeZone){
        //Sets the  defaultTime to a specific date, time and timezone.
        ZoneId zoneId = ZoneId.of("Europe/London");
        ZonedDateTime defaultTime= ZonedDateTime.of(2023, 5, 30, 18, 30, 0, 0, zoneId);

        ZoneId zone = ZoneId.of(timeZone);
        ZonedDateTime time3 = defaultTime.withZoneSameInstant(zone);

        LocalDateTime localTime3 = time3.toLocalDateTime();
        System.out.println("Current local time in "+timeZone+" timezone: " + localTime3);

        TimeZoneData x = new TimeZoneData();
        x.setTimezone(timeZone);
        x.setstrTime(localTime3.getHour() + ":"+ localTime3.getMinute());
        return x;
    }
}