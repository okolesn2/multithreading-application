package edu.wgu.d387_sample_code;
import edu.wgu.d387_sample_code.model.Translator;
import org.springframework.core.io.ClassPathResource;
import java.io.InputStream;
import java.util.Properties;
//The FrenchThread class  runs a new thread .
public class FrenchThread implements Runnable {
    //The Translator object holds 3 strings and contains getter and setter functions.
    public static volatile Translator information = new Translator();
    //The run method creates a new thread and  sets the name, thread ID and the resource bundle message of the Translator object.
    @Override
    public void run()
    {
        try
        {
            Thread.sleep(0);
            information.setMessage(resourceHandlerFR());
            information.setThreadName(Thread.currentThread().getName());
            information.setIdName(String.valueOf(Thread.currentThread().getId()));
            System.out.println("THREAD: Thread Name: "+ information.getThreadName() +" Thread ID: "+ information.getIdName() + " Message: " +information.getMessage());
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    // The resourceHandlerFR method loads the French resource bundle and returns a welcome message in English as a string.
    public static String resourceHandlerFR(){
        String welcome2 ;
        Properties properties = new Properties();
        try{
            InputStream stream = new ClassPathResource("welcome_fr_CA.properties").getInputStream();
            properties.load(stream);
            //System.out.println(properties.getProperty("welcome"));
            welcome2 =properties.getProperty("welcome");
            return welcome2;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    // The getValueFrench method returns information about the current thread.
    public static Translator getValueFrench() {
        //System.out.println("RETURN English: Thread Name: "+ information.getThreadName() +" Thread ID: "+ information.getIdName() + " Message: " +information.getMessage());
        return information;
    }
}
