
FROM openjdk:18

RUN mkdir -p /app

WORKDIR /app

COPY ./target/D387_sample_code-0.0.2-SNAPSHOT.jar ./app.jar

EXPOSE 8080

CMD ["java","-jar","./app.jar"]

# To create a executable and original JAR file:
# $ mvn clean package

# To build a image of spring boot application
# $ docker build -t  d387_003851444 .
