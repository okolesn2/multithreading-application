D3.  Describe how you would deploy the current multithreaded Spring application to the cloud.
Include the name of the cloud service provider you would use.]

Introduction
The current application can be deployed to the cloud using Amazon Web Services (AWS).
To deploy the application, I will first create a docker image of the application, then
tag and upload the docker image to an Amazon registry using Amazon Elastic Container
Registry (ECR). Then I will use Amazon Elastic Container Service (ECS) to create
a Cluster and a Task. The Cluster will specify the hardware resources to deploy a
docker container. And the Task will define the parameters of the docker container.
Lastly, I will use Amazon Fargate as the serverless computing engine to run the docker container.

Prerequisite
The following are the prerequisites to deploying the application using AWS :
The developer must install the AWS Command Line Interface on their local machine.
The developer must have an AWS account configured with an AIM user account with permission to access ECR, ECS, and
Fargate services.

Deployment instructions
The following steps describe the process of deploying the current application as a docker image using AWS:

1. Build a docker image of the current application by navigating to the root folder of the application containing the
docker file and enter the following command in the command terminal:
$ docker build -t DOCKER-IMAGE-NAME .
For example:
$ docker build -t d387-image .

2. Log in to the ECR by entering the following command in the command terminal :
$ aws ecr get-login-password --region YOUR-REGION | docker login --username AWS --password-stdin YOUR-ACCOUNT-ID.dkr.ecr.YOUR-REGION.amazonaws.com
For example:
aws ecr get-login-password --region us-west-1 | docker login --username AWS --password-stdin 960604111626.dkr.ecr.us-west-1.amazonaws.com

3. Tag the docker image by entering the following command in the command terminal:
$ docker tag DOCKER-IMAGE-NAME:latest YOUR-ACCOUNT-ID.dkr.ecr.YOUR-REGION.amazonaws.com/YOUR-ECR-REPO:YOURTAG
For example:
$ docker tag d387-image:latest 960604111626.dkr.ecr.us-west-1.amazonaws.com/d387-ecr:latest

4. Upload the docker image to ECR by entering the following command in the command terminal:
$ docker push YOUR-ACCOUNT-ID.dkr.ecr.YOUR-REGION.amazonaws.com/YOUR-ECR-REPO:YOURTAG
For example:
$ docker push 960604111626.dkr.ecr.us-west-1.amazonaws.com/d387-ecr:latest

5. Create a Cluster by logging into an AWS account, navigating to the  ECS home page, and clicking "Create Cluster"
from the menu. At the top of Create Cluster page, enter a Cluster name. Next, under the "Instance Type" label, select
Fargate. Then, under the "Infrastructure" label, select an operating system. Finally, click "Create" to create the
Cluster.

6. Create a new Task by logging into an AWS account, navigating to the ECS home page, and clicking "Task Definitions"
from the menu. Then select "Create new task definition" in the top right corner. Next, enter a name for the Task on the
task definition creation page under the "Task Definition Family" label. Next, under the "Container 1" label, specify a
container image name, enter the URI of the docker image stored in the ECR, and set the container port to 8080. Finally,
 click "Create" to create the Task Definition.

7. To connect the Task Definition to a Cluster and deploy the application, navigate to the ECS page and select
"Clusters" from the menu, then open the newly created Cluster from step 6, then navigate to the 'Tasks' tab, and select
"Run New Task". Next, under the "Deployment Configuration" heading under the "Family" label on the task creation page,
 select the Task Definition Family name created in step 6 and click "Create".

8. To test that the application is deployed and running successfully, navigate to the ECS home page and select
"Clusters" from the menu, open the created Cluster from step 6, click the 'Tasks' tab, and select the Task currently
running. Then navigate to the " Network Binding" tab and click on the "open address" link under the "External Link"
label to open the application. A new browser window will open, running the application.




